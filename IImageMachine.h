#ifndef IImageMachine_H
#define IImageMachine_H

#include <windows.h>

DECLARE_INTERFACE (IImageMachine)
{
public:

	virtual void Start();
};

//-- Export functions ----------------------------------------------------
IImageMachine* GetImageMachine();
HRESULT ReleaseImageMachine(IImageMachine* ImageObj);

//-- Define type of export functions -------------------------------------
typedef IImageMachine* (* kvsGetImageMachine)();
typedef HRESULT (* kvsReleaseImageMachine)(IImageMachine* ImageObj);


#endif 
