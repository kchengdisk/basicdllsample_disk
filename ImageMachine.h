#ifndef ImageMachine_H
#define ImageMachine_H

#include "IImageMachine.h"

class ImageMachine: public IImageMachine
{
public:
	ImageMachine();
	virtual ~ImageMachine();

	void Start();

};

#endif